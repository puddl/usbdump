# design
initial design: udev rule --> trigger rsync

udev kann nur kurze runs https://askubuntu.com/questions/667922/udev-script-doesnt-run-in-the-background

socket? https://unix.stackexchange.com/questions/454731/systemd-socket-activation-trigger-a-bash-script
spool? https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch05s14.html

uh! oneshot service, also `udev rule --> usbdump service start`

das isses: https://askubuntu.com/a/679600/36078
https://serverfault.com/a/767079/401207


# references
- https://help.ubuntu.com/community/UsbDriveDoSomethingHowtohttps://help.ubuntu.com/community/UsbDriveDoSomethingHowto
- https://www.tecmint.com/udev-for-device-detection-management-in-linux/
- best: https://serverfault.com/a/767079/401207
- https://serverfault.com/questions/766506/automount-usb-drives-with-systemd


# quality
works for me

ran [shellcheck](https://github.com/koalaman/shellcheck); only returned
```
SC2086: Double quote to prevent globbing and word splitting.
```


# hama ftw
https://devicehunt.com/view/type/usb/vendor/0BDA/device/0109 ;)
