# prerequisites
- bash
- rsync
- systemd

exFAT support
```
sudo apt-get install exfat-fuse exfatprogs
```


# getting started
First, check your USB drive. Before plugging it in run
```
tail -f /var/log/syslog
```

You should see something like this:
```
Sep 29 21:23:16 plato kernel: [1534381.159375] usb 3-9: New USB device found, idVendor=0bda, idProduct=0109
[...]
Sep 29 21:23:17 plato kernel: [1534382.935932] sd 10:0:0:0: [sdh] Assuming drive cache: write through
Sep 29 21:23:17 plato kernel: [1534382.938189]  sdh: sdh1
```

Vendor id and product id uniquely identify your thumb drive. To double check run
```
udevadm info /dev/sdh1 | egrep 'ID_(MODEL|VENDOR)'
```

For my drive this returned
```
E: ID_MODEL_ID=0109
E: ID_VENDOR_ID=0bda
```


# install
```
# set your target location
$EDITOR usbdump
# set your user:group pair (in my case this is felix:felix)
$EDITOR usbdump@.service
# be sure to set ID_MODEL_ID and ID_VENDOR_ID
$EDITOR 91-usbdump.rules

sudo cp usbdump /usr/local/bin/usbdump
sudo cp usbdump@.service /etc/systemd/system/usbdump@.service
sudo cp 91-usbdump.rules /etc/udev/rules.d/91-usbdump.rules
sudo install -D --mode 644 include-patterns /etc/usbdump/include-patterns

sudo systemctl daemon-reload
sudo udevadm control --reload-rules
```


# usage
Plug your drive. Watch status:
```
systemctl list-units 'usbdump*'
journalctl -u usbdump@sdh1.service
```


# run once
```
sudo -Hn usbdump /dev/sdh1 felix:felix
```
