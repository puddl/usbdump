#!/bin/bash
set -euo pipefail

cp /etc/usbdump/include-patterns include-patterns
cp /usr/local/bin/usbdump usbdump
cp /etc/systemd/system/usbdump@.service usbdump@.service
cp /etc/udev/rules.d/91-usbdump.rules 91-usbdump.rules
